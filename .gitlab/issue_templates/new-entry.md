# New Entry Template

## Section

<!-- Check one with an x. -->

* [ ]  Extension
* [ ]  Script
* [ ]  CSS Theme

## Information

### Required

<!-- If your entry is a closed source extension you have a significantly higher chance to be denied as we want to be able to see what your extension does. -->

* Project name:
* Project link:
* Author name:
* Author link:
* Description:

### Optional

<!-- Add official links only. Any obscure/sketchy links will not be included and may even cause your entry to be denied entirely. -->

* Webstore links:
  * Chrome:
  * Firefox:
  * Opera:
  * ...
* Snippet (raw text) link:
* Userstyles link:
* ...
