# Relocated

This project is no longer being maintained and has been migrated over to the [Tildes Unofficial Wiki](https://unofficial-tildes-wiki.gitlab.io/tech/customizing-tildes.html).